var express = require('express')
var cas = require('connect-cas')
var url = require('url')
var bodyParser = require('body-parser')
var axios = require('axios')
const path = require('path')
const fs = require('fs')
const cookieParser = require('cookie-parser')
const cookieSession = require('cookie-session')
const apiKey = process.env.FORM_KEY
const cookieKey = process.env.COOKIE_SECRET
const getEndpoint = process.env.GET_ENDPOINT
const postEndpoint = process.env.POST_ENDPOINT

// CAS server's hostname
cas.configure({ 'host': 'login.vt.edu/profile' })
var app = express()

app.use(bodyParser.json())
app.use(cookieParser(cookieKey))
app.use(cookieSession({ secret: cookieKey }))
app.enable('trust proxy')
app.use('/', cas.serviceValidate(), cas.authenticate(), express.static(path.join(__dirname, 'build')))
console.log(__dirname)

app.get('/api', cas.serviceValidate(), cas.authenticate(), async function(req, res) {
  if (req.session.cas && req.session.cas.user) {
    var data = await axios.get(getEndpoint + req.session.cas.user, {
      headers: {
        'Accept': 'application/json',
        'X-DreamFactory-Api-Key': apiKey
      }
    }).then( async (response) => {
      if (response.status === 200 && response.data.resource !== undefined && response.data.resource.length > 0) {
        console.log(response.status)
        return await {students: filterResults(response), instructor: response.data.resource[0].id}
      } else if (response.status === 200) {
        return {pid: false}
      } else {
        return {error: "API Error"}
      }
    }).then((data) => {
      return res.send(data)
    })
  }
})

app.post('/send', cas.serviceValidate(), cas.authenticate(), async function (req, res) {
  if (req.session.cas && req.session.cas.user) {
    var data = await axios({
      method: 'POST',
      url: postEndpoint,
      headers: {
        'Accept': 'application/json',
        'X-DreamFactory-Api-Key': apiKey,
        'Content-Type': 'application/json'
      },
      data: {
        resource: [
          {
            name: req.body.name,
            instructor_id: req.body.instructor_id,
          }
        ]
      }
    }).then((response) => {
      console.log(response.status)
      if (response.status === 200) {
        res.sendStatus(200)
      } else {
        res.sendStatus(500)
      }
    }).catch((error) => {
      console.log(error)
      res.sendStatus(500)
    })
  }
})

// This route has the serviceValidate middleware, which verifies
// that CAS authentication has taken place, and also the
// authenticate middleware, which requests it if it has not already
// taken place.

app.get('/login', cas.serviceValidate(), cas.authenticate(), function (req, res) {
  return res.redirect('/')
})

app.get('/logout', function (req, res) {
  if (!req.session) {
    return res.redirect('/')
  }
  // Forget our own login session
  if (req.session.destroy) {
    req.session.destroy()
  } else {
    // Cookie-based sessions have no destroy()
    req.session = null
  }
  // Send the user to the official campus-wide logout URL
  var options = cas.configure()
  options.pathname = options.paths.logout
  return res.redirect(url.format(options))
})

app.listen(3000, () => console.log("Server running on port 3000"))

function filterResults(response) {
  return response.data.resource[0]['student_by_instructor_id'].map((value,index) => {
    return {
      id: value.id,
      timestamp_create: value.timestamp_create,
      name: value.name,
      score: value.score,
      possible: value.possible,
      course_id: response.data.resource[0]['course_by_instructor_id'].find((element) => {
        return element.id === value.course_id
      }) 
    }
  })
}
